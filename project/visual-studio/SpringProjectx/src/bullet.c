#include "common.h"
#include "structs.h"
#include "player.c"

static SDL_Texture* pete[2];

void initBullets(void)
{
	memset(&stage.bulletHead, 0, sizeof(Bullet));
	stage.bulletTail = &stage.bulletHead;

}

Bullet* spawnBullet(Entity* owner)
{
	Bullet* b;

	b = malloc(sizeof(Bullet));
	memset(b, 0, sizeof(Bullet));
	stage.bulletTail->next = b;
	stage.bulletTail = b;

	pete[2] = loadTexture("gfx/pete03.png");
	pete[3] = loadTexture("gfx/attack1.png");
	b->texture = pete[3];

	b->owner = owner;

	return b;
}

void doBullet(void)
{
	Bullet* b;
	Gunner* g;

	g = (Gunner*)self->data;

	g->reload = MAX(g->reload - app.deltaTime, 0);

	if (app.keyboard[SDL_SCANCODE_J] && g->reload == 0)
	{
		b = spawnBullet(self);
		b->texture = pete[4];
		b->life = FPS * 2;


		if (g->aimingUp)
		{
			b->y = self->y - b->texture->rect.h;
			b->dy = -BULLET_SPEED;

			if (self->dx == 0)
			{
				b->x = player->x + (b->texture->pete[3] / 2) - (b->texture->pete[4] / 2);
				b->x += player->texture = pete[4]; ? 12 : -12;
			}
			else
			{
				b->x = player->texture == pete[3] ? self->x + player->texture->pete[4] : self->x;
				b->dx = self->texture == pete[3] ? BULLET_SPEED : -BULLET_SPEED;
			}
		}
		else
		{
			b->x = player->texture == pete[3] ? player->x + player->texture->pete[4] : self->x;
			b->y = player->y + 19;
			b->dx = player->texture == pete[3] ? BULLET_SPEED : -BULLET_SPEED;
		}

		g->reload = RELOAD_SPEED;
	}
}