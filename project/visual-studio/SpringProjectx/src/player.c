/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static SDL_Texture* pete[2];

void initPlayer(void)
{
	Gunner* g;
	g = malloc(sizeof(Gunner));
	memset(g, 0, sizeof(Gunner));

	player = malloc(sizeof(Entity));
	memset(player, 0, sizeof(Entity));
	stage.entityTail->next = player;
	stage.entityTail = player;

	g->animTimer = ANIME_TIME;

	player->health = 1;

	pete[0] = loadTexture("gfx/pete01.png");
	pete[1] = loadTexture("gfx/pete02.png");
	pete[3] = loadTexture("gfx/pete03.png");
	pete[4] = loadTexture("gfx/attack1.png");

	player->texture = pete[0];

	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);



	
	}

	

void doPlayer(void)
{
	player->dx = 0;
	
	Gunner* g;

	g = (Gunner*)player-> data;

	g->aimingUp = 0;


	if (app.keyboard[SDL_SCANCODE_A])
	{
		player->dx = -PLAYER_MOVE_SPEED;

		player->texture = pete[1];
	}

	if (app.keyboard[SDL_SCANCODE_D])
	{
		player->dx = PLAYER_MOVE_SPEED;

		player->texture = pete[0];
	}

	if (app.keyboard[SDL_SCANCODE_W] && player->isOnGround)
	{
		player->riding = NULL;

		player->dy = -20;

		playSound(SND_JUMP, CH_PLAYER);
	}

	if (app.keyboard[SDL_SCANCODE_SPACE])
	{
		player->x = player->y = 0;

		app.keyboard[SDL_SCANCODE_SPACE] = 0;
	}


	if (app.keyboard[SDL_SCANCODE_I])
	{
		g->aimingUp = 1;
	}

	if (self->dx != 0)
		{
		g->animTimer -= app.deltaTime;

	if (g->animTimer <= 0)
		{
		g->frame = (g->frame + 1);
		player->texture = pete[4];

			g->animTimer = ANIME_TIME;
		}

	if (g->aimingUp)
		{
		player->texture = pete[3];
		g->frame;
		}
		else
		{
		player->texture = pete[1];
		g->frame;
			}
		}
	else
	{
		g->frame = 0;

		if (g->aimingUp)
		{
			player->texture = pete[3];
		}
		else
		{
			player->texture = pete[1];
		}

	}

}
