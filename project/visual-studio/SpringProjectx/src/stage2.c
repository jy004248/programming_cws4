#include "common.h"

static void logic2(void);
static void draw2(void);
static void drawHud2(void);

void initStage2(void)
{
	app.delegate.logic = logic2;
	app.delegate.draw = draw2;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;

	initEntities();

	initPlayer();

	initMap2();
}

static void logic2(void)
{
	doPlayer();

	doEntities();

	doCamera();
}

static void draw2(void)
{
	SDL_SetRenderDrawColor(app.renderer, 128, 0, 255, 255);
	SDL_RenderFillRect(app.renderer, NULL);

	drawMap2();

	drawEntities();

	drawHud2();
}

static void drawHud2(void)
{
	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
	SDL_RenderFillRect(app.renderer, &r);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	drawText(SCREEN_WIDTH - 5, 5, 255, 255, 255, TEXT_RIGHT, "PIZZA %d/%d", stage.pizzaFound, stage.pizzaTotal);
}
