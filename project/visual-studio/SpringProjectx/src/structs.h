/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/


typedef struct Texture Texture;
typedef struct Entity Entity;
typedef struct Bullet Bullet;

typedef struct {
	void (*logic)(void);
	void (*draw)(void);
} Delegate;

struct Texture {
	char name[MAX_NAME_LENGTH];
	SDL_Texture *texture;
	Texture *next;
};

typedef struct {
	SDL_Renderer *renderer;
	SDL_Window *window;
	Delegate delegate;
	int keyboard[MAX_KEYBOARD_KEYS];
	Texture textureHead, *textureTail;
	int deltaTime;
} App;

struct Entity {
	float x;
	float y;
	int w;
	int h;
	float ex;
	float ey;
	float sx;
	float sy;
	float dx;
	float dy;
	int health;
	int isOnGround;
	float value;
	SDL_Texture *texture;
	void (*tick)(void);
	void (*touch)(Entity *other);
	long flags;
	int facing;
	int onGround;
	void(*data);
	void (*draw)(Entity* self);
	Entity* next;
	Entity *riding;
};

typedef struct {
	SDL_Point camera;
	int map[MAP_WIDTH][MAP_HEIGHT];
	int level2[MAP_WIDTH][MAP_HEIGHT];
	Entity entityHead, *entityTail;
	Entity bulletHead, * bulletTail;
	int pizzaTotal, pizzaFound;
} Stage;


typedef struct {
	int frame;
	int aimingUp;
	double animTimer;
	double reload;
} Gunner;

 struct Bullet  {
	double x;
	double y;
	double dx;
	double dy;
	double life;
	SDL_Texture  *texture;
	Entity *owner;
	Bullet *next;
  };
